//
//  ProductsLogic.m
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import "ProductsLogic.h"
#import "AFURLSessionManager.h"
#import "AFNetworking.h"

@implementation ProductsLogic

+(void)getProductsWithCategoryId:(NSString *)cat_Id withSuccess:(void (^)(NSDictionary *))success andFailure:(void (^)(void))failure {

    NSString *string = @"http://kasb.azurewebsites.net/Api/GetProductByCatId";
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSDictionary *params =@{@"categoryId": cat_Id};
    
    [manager POST: string parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {

        success(responseObject);
    
    } failure:^(NSURLSessionTask *operation, NSError *error) {

        failure();
    }];
}

+(void)getProductDetailsWithProductId:(NSString *)prod_Id withSuccess:(void (^)(NSDictionary *))success andFailure:(void (^)(void))failure {
    
    
    NSString *string = @"http://kasb.azurewebsites.net/Api/GetProductsDetails";
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSDictionary *params =@{ @"productId": prod_Id };
    
    [manager POST: string parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        success(responseObject);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        
        failure();
    }];
}
@end
