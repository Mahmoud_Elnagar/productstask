//
//  ProductsLogic.h
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductsLogic : NSObject


+ (void)getProductsWithCategoryId:(NSString *)cat_Id withSuccess:(void (^)(NSDictionary *data)) success andFailure:(void (^)(void)) failure;

+ (void)getProductDetailsWithProductId:(NSString *)prod_Id withSuccess:(void (^)(NSDictionary *Product)) success andFailure:(void (^)(void)) failure;
@end
