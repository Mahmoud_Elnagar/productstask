//
//  Product.h
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (strong, nonatomic) NSNumber *prod_Id;
@property (strong, nonatomic) NSString *prod_Image_Url;
@property (strong, nonatomic) NSString *prod_Name;
@property (strong, nonatomic) NSString *prod_Material;
@property (strong, nonatomic) NSNumber *prod_Quantity;
@property (strong, nonatomic) NSString *prod_Category_Name;
@property (strong, nonatomic) NSString *prod_Description;
@property (strong, nonatomic) NSString *prod_Price;

- (Product *)initWithDictionary:(NSDictionary *)dic;
@end
