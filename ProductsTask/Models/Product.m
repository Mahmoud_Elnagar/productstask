//
//  Product.m
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import "Product.h"

@implementation Product



- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if (self) {
        if ([dict objectForKey:@"Id"]) {
            self.prod_Id = [dict objectForKey:@"Id"];
        }
        
        if ([dict objectForKey:@"Image"]) {
            self.prod_Image_Url = [dict objectForKey:@"Image"];
        }
        
        if ([dict objectForKey:@"Name"]){
            self.prod_Name = [dict objectForKey:@"Name"];
        }
        
        if ([dict objectForKey:@"Material"]) {
            self.prod_Material = [dict objectForKey:@"Material"];
        }
        
        if ([dict objectForKey:@"Quantity"]) {
            self.prod_Quantity = [dict objectForKey:@"Quantity"];
        }
        
        if ([dict objectForKey:@"QuantityInStock"]) {
            self.prod_Quantity = [dict objectForKey:@"QuantityInStock"];
        }
        
        if ([dict objectForKey:@"CategoryName"]) {
            self.prod_Category_Name = [dict objectForKey:@"CategoryName"];
        }
        
        if ([dict objectForKey:@"Description"]) {
            self.prod_Description = [dict objectForKey:@"Description"];
        }
        
        if ([dict objectForKey:@"price"]) {
            self.prod_Price = [dict objectForKey:@"price"];
        }
        
    }
    return self;
    
}


@end
