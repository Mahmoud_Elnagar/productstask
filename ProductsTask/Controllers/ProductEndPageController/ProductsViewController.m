//
//  ProductsViewController.m
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import "ProductsViewController.h"
#import "ProductEndPageController.h"
#import "ProductCell.h"
#import "MBProgressHUD.h"

@interface ProductsViewController ()

@end

@implementation ProductsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initPresenter];
    
    _products = [NSArray new];
}

-(void) initPresenter {
    
     __weak __typeof(self)weakSelf = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    _presenter = [ProductsPresenter new];
    [_presenter setReturnProductsResponseBlock:^(NSArray * prods) {
        [MBProgressHUD hideHUDForView:weakSelf.view animated:true];
        weakSelf.products = prods;
        [weakSelf.tableView reloadData];
    }];
    
    [_presenter setReturnFailureBlock:^{
        
        [MBProgressHUD hideHUDForView:weakSelf.view animated:true];
    }];
    
    [_presenter getProducts];
}


#pragma Mark - tableview datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  self.products.count;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProductCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"ProductCell"];
    Product *model = [self.products objectAtIndex:indexPath.section];
    
    [cell initData:model];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    Product *model = [self.products objectAtIndex:indexPath.section];
    ProductEndPageController * endPAge = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductEndPageController"];
    endPAge.productId = model.prod_Id;
    [self.navigationController pushViewController:endPAge animated:true];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  90;
}


@end
