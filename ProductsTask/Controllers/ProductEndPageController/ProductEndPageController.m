//
//  ProductEndPageController.m
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import "ProductEndPageController.h"
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"

@interface ProductEndPageController ()

@end

@implementation ProductEndPageController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initPresenter];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 30.0f, 30.0f)];
    [backButton setImage:[UIImage imageNamed:@"back_icon"]  forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
}

-(void) initPresenter {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:true];
    __weak __typeof(self)weakSelf = self;
    
    _presenter = [ProductsPresenter new];
    
    [_presenter setReturnProductDetailsResponseBlock:^(Product *prod) {
        
        
        [weakSelf.prod_Image sd_setImageWithURL:[NSURL URLWithString:prod.prod_Image_Url] placeholderImage:[UIImage imageNamed:@"placeHolder"]];
        
        weakSelf.prod_Name.text = prod.prod_Name;
        weakSelf.prod_Category.text = prod.prod_Category_Name;
        
        weakSelf.prod_Matertial.text = prod.prod_Material;
        
        NSData * htmlData = [prod.prod_Description dataUsingEncoding:NSUnicodeStringEncoding];
        
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: htmlData
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        weakSelf.product_Description.attributedText = attributedString;
        
        [MBProgressHUD hideHUDForView:weakSelf.view animated:true];
    }];
    
    [_presenter setReturnFailureBlock:^{
        
        [MBProgressHUD hideHUDForView:weakSelf.view animated:true];
    }];
    
    [_presenter getProductDetailsWithProdId:[_productId stringValue]];
}

-(IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES ];
}

@end
