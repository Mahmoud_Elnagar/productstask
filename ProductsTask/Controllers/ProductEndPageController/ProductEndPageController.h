//
//  ProductEndPageController.h
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductsPresenter.h"

@interface ProductEndPageController : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *prod_Image;
@property (weak, nonatomic) IBOutlet UILabel *prod_Name;
@property (weak, nonatomic) IBOutlet UILabel *prod_Category;
@property (weak, nonatomic) IBOutlet UILabel *prod_Matertial;
@property (weak, nonatomic) IBOutlet UITextView *product_Description;
@property (strong , nonatomic) NSNumber *productId;
@property ProductsPresenter *presenter;
@end
