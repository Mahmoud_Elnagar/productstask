//
//  ProductsViewController.h
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductsPresenter.h"

@interface ProductsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong , nonatomic) NSArray *products;
@property ProductsPresenter *presenter;
@end
