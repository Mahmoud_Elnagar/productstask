//
//  ProductCell.h
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"

@interface ProductCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *product_Image_View;
@property (weak, nonatomic) IBOutlet UILabel *product_Title;
@property (weak, nonatomic) IBOutlet UILabel *product_Quantity;

-(void)initData:(Product *) product;
@end
