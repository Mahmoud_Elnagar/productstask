//
//  ProductCell.m
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import "ProductCell.h"
#import "Product.h"
#import "UIImageView+WebCache.h"

@implementation ProductCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

-(void) initData:(Product *) product {

    [_product_Image_View sd_setImageWithURL:[NSURL URLWithString:product.prod_Image_Url]  placeholderImage:[UIImage imageNamed:@"placeHolder"]];
    
    _product_Quantity.text = [product.prod_Quantity stringValue];
    _product_Title.text = product.prod_Name;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
