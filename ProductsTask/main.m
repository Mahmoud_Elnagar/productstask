//
//  main.m
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
