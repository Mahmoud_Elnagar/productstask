//
//  ProductsPresenter.h
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

@interface ProductsPresenter : NSObject


@property (nonatomic, copy) void (^returnProductsResponseBlock)(NSArray * response);
@property (nonatomic, copy) void (^returnProductDetailsResponseBlock)(Product * product);
@property (nonatomic, copy) void (^returnFailureBlock)(void);

-(void) getProducts;
-(void) getProductDetailsWithProdId:(NSString*) prod_Id;
@end
