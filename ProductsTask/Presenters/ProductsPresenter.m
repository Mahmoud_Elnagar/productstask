//
//  ProductsPresenter.m
//  ProductsTask
//
//  Created by Mahmoud on 7/19/17.
//  Copyright © 2017 Mahmoud Elnagar. All rights reserved.
//

#import "ProductsPresenter.h"
#import "ProductsLogic.h"

@implementation ProductsPresenter

-(void) getProducts{
    
    [ProductsLogic getProductsWithCategoryId:@"1" withSuccess:^(NSDictionary *data) {
        
        NSString *statusCode = [data objectForKey:@"StatusCode"];
        
        if ([statusCode isEqual:@200]) {
            
            NSArray *result = [(NSDictionary*)data objectForKey:@"data"];
            if (result) {
                
                NSMutableArray *productsArray = [NSMutableArray new];
                
                for (NSDictionary *prod in result) {
                    
                    Product *product = [[Product alloc] initWithDictionary:prod];
                    [productsArray addObject: product];
                }
                
                _returnProductsResponseBlock(productsArray);
            }
        }else{
            _returnFailureBlock();
        }
        
    } andFailure:^{
        _returnFailureBlock();
    }];
}


-(void) getProductDetailsWithProdId:(NSString*) prod_Id {
    
    [ProductsLogic getProductDetailsWithProductId:prod_Id withSuccess:^(NSDictionary *data) {
        
        NSString *statusCode = [data objectForKey:@"StatusCode"];
        if ([statusCode isEqual:@200]) {
            
            NSDictionary *dataDic = [data objectForKey:@"data"];
            if (dataDic) {
                Product *product = [[Product alloc] initWithDictionary:dataDic];
                _returnProductDetailsResponseBlock(product);
            }
        }else {
            _returnFailureBlock();
        }
        
    } andFailure:^{
        _returnFailureBlock();
    }];
}



@end
